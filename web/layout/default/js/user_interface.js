$( document ).ready( function() {
    window.oUserListPagination = {
        limit: 20,
        page: 1,
        pages: 1,
        total: 0
    }
    
    $('#api_users_users_list_search_user').on('click', function(event) {
        var sSearchedUserName = $('#api_users_users_list_username').val();
        
        $.sendGetUsersListByUserNameAjaxRequest(sSearchedUserName);
    });
    
    $('#api_users_users_list_get_all_users').on('click', function(event) {
        $.sendGetUsersListAllUsersAjaxRequest();
    });

    $('#api_users_users_list_clear_table').on('click', function(event) {
        window.oUserListPagination = {
            limit: 20,
            page: 1,
            pages: 1,
            total: 0
        }

        $.clearUserListTable();
    });

    $.addOnClickEventForEditButtons = function() {
        var oEditButtons = $("button[id^='button-edit-user']");
        
        oEditButtons.on('click', function(event) {
            var iUserId = $(this).attr('data-user-id');
            
            $.hideAllUpdateButtons();
            $.hideAllUserListInputs();
            $.hideAllUserListSelects();

            $.showAllEditButtons();

            $('#button-edit-user-' + iUserId).hide();
            $('#button-update-user-' + iUserId).show();

            $.createEditFormForUser(iUserId);
        });
    }

    $.addOnClickEventForUpdateButtons = function() {
        var oUpdateButtons = $("button[id^='button-update-user']");
        
        oUpdateButtons.on('click', function(event) {
            var iUserId = $(this).attr('data-user-id');

            $('#button-update-user-' + iUserId).hide();
            $('#button-edit-user-' + iUserId).show();

            var oUser = {
                id: iUserId,
                name: $('#input-user-' + iUserId + '-name').val(),
                email: $('#input-user-' + iUserId + '-email').val(),
                gender: $('#select-user-' + iUserId + '-gender').val(),
                status: $('#select-user-' + iUserId + '-status').val()
            }

            $.hideAllUserListInputs();
            $.hideAllUserListSelects();

            $.sendUpdateUserAjaxRequest(oUser);
        });
    }
    
    $('#api_users_users_list_preview').on('click', function(event) {        
        if (window.oUserListPagination.page == 1) {
            return;
        }

        $.sendGetUsersListPreviewPageAjaxRequest();
    });

    $('#api_users_users_list_next').on('click', function(event) {
        if (window.oUserListPagination.page == window.oUserListPagination.pages) {
            return;
        }

        $.sendGetUsersListNextPageAjaxRequest();
    });
});
