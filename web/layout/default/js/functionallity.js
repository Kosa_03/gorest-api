$( document ).ready( function() {
    $.updateUserListPaginationFromAjaxResponse = function(oResponsePagination) {
        window.oUserListPagination = {
            limit: oResponsePagination.limit,
            page: oResponsePagination.page,
            pages: oResponsePagination.pages,
            total: oResponsePagination.total
        };

        $.updatePaginationPanel();
    }

    $.clearUserListTable = function(oPagination) {
        $('#users-list-table > tr').remove();

        $.updatePaginationPanel();
    }

    $.updatePaginationPanel = function() {
        $('#user-list-pagination-current-page').text(window.oUserListPagination.page);
        $('#user-list-pagination-pages').text(window.oUserListPagination.pages);
        $('#user-list-pagination-total').text(window.oUserListPagination.total);
    }

    $.appendUserToUserListTable = function(oUser) {
        var oTBody = $('#users-list-table');

        var sTableRow = '<tr>'
                      +   '<td id="user-' + oUser.id + '-name">' + oUser.name + '</td>'
                      +   '<td id="user-' + oUser.id + '-email">' + oUser.email + '</td>'
                      +   '<td id="user-' + oUser.id + '-gender">' + oUser.gender + '</td>'
                      +   '<td id="user-' + oUser.id + '-status">' + oUser.status + '</td>'
                      +   '<td>' + oUser.created_at + '</td>'
                      +   '<td>' + oUser.updated_at + '</td>'
                      +   '<td class="d-flex justify-content-center">'
                      +     $.createEditButton(oUser.id)
                      +     $.createUpdateButton(oUser.id)
                      +   '</td>'
                      + '</tr>'
        ;

        oTBody.append(sTableRow);

        $.hideAllUpdateButtons();
    }

    $.createEditFormForUser = function(iUserId) {
        var oUserNameTableCell = $('#user-' + iUserId + '-name');
        var oUserEmailTableCell = $('#user-' + iUserId + '-email');
        var oUserGenderTableCell = $('#user-' + iUserId + '-gender');
        var oUserStatusTableCell = $('#user-' + iUserId + '-status');

        var sUserName = oUserNameTableCell.text();
        var sUserEmail = oUserEmailTableCell.text();
        var sUserGender = oUserGenderTableCell.text();
        var sUserStatus = oUserStatusTableCell.text();

        oUserNameTableCell.text('').append($.createInputUserName(iUserId, sUserName));
        oUserEmailTableCell.text('').append($.createInputUserEmail(iUserId, sUserEmail));
        oUserGenderTableCell.text('').append($.createSelectUserGender(iUserId, sUserGender));
        oUserStatusTableCell.text('').append($.createSelectUserStatus(iUserId, sUserStatus));
    }

    $.showAllEditButtons = function() {
        var oEditButtons = $("button[id^='button-edit-user']");
        oEditButtons.show();
    }

    $.hideAllEditButtons = function() {
        var oEditButtons = $("button[id^='button-edit-user']");
        oEditButtons.hide();
    }

    $.hideAllUpdateButtons = function() {
        var oUpdateButtons = $("button[id^='button-update-user']");
        oUpdateButtons.hide();
    }

    $.hideAllUserListInputs = function() {
        var oFormInputs = $("input[id^='input-user']");

        oFormInputs.each( function() {
            var oParent = $(this).parent();
            var sValue = $(this).val();

            oParent.text(sValue);
        });

        oFormInputs.remove();
    }

    $.hideAllUserListSelects = function() {
        var oFormSelects = $("select[id^='select-user']");

        oFormSelects.each( function() {
            var oParent = $(this).parent();
            var sValue = $(this).val();

            oParent.text(sValue);
        });

        oFormSelects.remove();
    }

    $.showAlert = function(sAlertType, sAlertMessage) {
        $('#user-list-container').prepend($.createAlert(sAlertType, sAlertMessage));
    }
});
