$( document).ready( function() {
    var sApiUrl = params.sApiUrl;
    var sApiKey = params.sApiKey;
    
    $.sendGetUsersListByUserNameAjaxRequest = function(sSearchedUserName) {
        $.ajax({
            url: sApiUrl,
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + sApiKey
            },
            data: {
                name: sSearchedUserName
            },
            success: function(data) {
                if (data.code != 200) {
                    $.showAlert('danger', 'Something went wrong. Status code: ' + data.code);
                    return;
                }

                var aData = data.data;

                $.updateUserListPaginationFromAjaxResponse(data.meta.pagination);

                $.clearUserListTable();
                $.each(aData, function(iKey, oUser) {
                    $.appendUserToUserListTable(oUser);
                });

                $.addOnClickEventForEditButtons();
                $.addOnClickEventForUpdateButtons();
            },
            error: function(xhr, status, error) {
                $.showAlert('danger', 'Response message "' + xhr.responseText + '"');
            }
        });
    }

    $.sendGetUsersListAllUsersAjaxRequest = function() {
        $.ajax({
            url: sApiUrl,
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + sApiKey
            },
            success: function(data) {
                if (data.code != 200) {
                    $.showAlert('danger', 'Something went wrong. Status code: ' + data.code);
                    return;
                }

                var aData = data.data;

                $.updateUserListPaginationFromAjaxResponse(data.meta.pagination);

                $.clearUserListTable();
                $.each(aData, function(iKey, oUser) {
                    $.appendUserToUserListTable(oUser);
                });

                $.addOnClickEventForEditButtons();
                $.addOnClickEventForUpdateButtons();
            },
            error: function(xhr, status, error) {
                $.showAlert('danger', 'Response message "' + xhr.responseText + '"');
            }
        });
    }

    $.sendGetUsersListPreviewPageAjaxRequest = function() {
        $.ajax({
            url: sApiUrl,
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + sApiKey
            },
            data: {
                page: window.oUserListPagination.page - 1,
            },
            success: function(data) {
                if (data.code != 200) {
                    $.showAlert('danger', 'Something went wrong. Status code: ' + data.code);
                    return;
                }

                var aData = data.data;

                $.updateUserListPaginationFromAjaxResponse(data.meta.pagination);

                $.clearUserListTable();
                $.each(aData, function(iKey, oUser) {
                    $.appendUserToUserListTable(oUser);
                });

                $.addOnClickEventForEditButtons();
                $.addOnClickEventForUpdateButtons();
            },
            error: function(xhr, status, error) {
                $.showAlert('danger', 'Response message "' + xhr.responseText + '"');
            }
        });
    }

    $.sendGetUsersListNextPageAjaxRequest = function(oPagination) {
        $.ajax({
            url: sApiUrl,
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + sApiKey
            },
            data: {
                page: window.oUserListPagination.page + 1,
            },
            success: function(data) {
                if (data.code != 200) {
                    $.showAlert('danger', 'Something went wrong. Status code: ' + data.code);
                    return;
                }
                
                var aData = data.data;

                $.updateUserListPaginationFromAjaxResponse(data.meta.pagination);

                $.clearUserListTable();
                $.each(aData, function(iKey, oUser) {
                    $.appendUserToUserListTable(oUser);
                });

                $.addOnClickEventForEditButtons();
                $.addOnClickEventForUpdateButtons();
            },
            error: function(xhr, status, error) {
                $.showAlert('danger', 'Response message "' + xhr.responseText + '"');
            }
        });
    }

    $.sendUpdateUserAjaxRequest = function(oUser) {
        $.ajax({
            url: sApiUrl + '/' + oUser.id,
            method: 'PATCH',
            headers: {
                'Authorization': 'Bearer ' + sApiKey
            },
            data: {
                name: oUser.name,
                email: oUser.email,
                gender: oUser.gender,
                status: oUser.status
            },
            success: function(data) {
                if (data.code == 200) {
                    $.showAlert('success', 'Successfully updated user "' + oUser.name + '"');
                } else {
                    $.showAlert('danger', 'Something went wrong. User not updated.');
                }
            },
            error: function(xhr, status, error) {
                $.showAlert('danger', 'Response message "' + xhr.responseText + '"');
            }
        });
    }
});
