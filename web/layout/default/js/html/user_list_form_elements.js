$( document ).ready( function() {
    $.createEditButton = function(iUserId) {
        return '<button id="button-edit-user-' + iUserId + '" class="btn btn-sm btn-outline-success" data-user-id="' + iUserId + '" type="button">'
             +   'Edit'
             + '</button>';
    }
    
    $.createUpdateButton = function(iUserId) {
        return '<button id="button-update-user-' + iUserId + '" class="btn btn-sm btn-outline-success" data-user-id="' + iUserId + '" type="button">'
             +   'Update'
             + '</button>';
    }
    
    $.createInputUserName = function(iUserId, sValue) {
        return '<input type="text" id="input-user-' + iUserId + '-name" name="input-user-' + iUserId + '-name" class="form-control form-control-sm" value="' + sValue + '">';
    }
    
    $.createInputUserEmail = function(iUserId, sValue) {
        return '<input type="text" id="input-user-' + iUserId + '-email" name="input-user-' + iUserId + '-email" class="form-control form-control-sm" value="' + sValue + '">';
    }
    
    $.createSelectUserGender = function(iUserId, sValue) {
        var sFemaleSelected = '';
        var sMaleSelected = '';
    
        if (sValue == 'Female') {
            sFemaleSelected = ' selected';
        }
        if (sValue == 'Male') {
            sMaleSelected = ' selected';
        }
    
        return '<select id="select-user-' + iUserId + '-gender" name="select-user-' + iUserId + '-gender" class="custom-select custom-select-sm">'
             +   '<option' + sFemaleSelected + '>Female</option>'
             +   '<option' + sMaleSelected + '>Male</option>'
             + '</select>';
    }
    
    $.createSelectUserStatus = function(iUserId, sValue) {
        var sActiveSelected = '';
        var sInactiveSelected = '';
    
        if (sValue == 'Active') {
            sActiveSelected = ' selected';
        }
        if (sValue == 'Inactive') {
            sInactiveSelected = ' selected';
        }
    
        return '<select id="select-user-' + iUserId + '-status" name="select-user-' + iUserId + '-status" class="custom-select custom-select-sm">'
             +   '<option' + sActiveSelected + '>Active</option>'
             +   '<option' + sInactiveSelected + '>Inactive</option>'
             + '</select>';
    }
});
