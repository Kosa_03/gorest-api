$( document ).ready( function() {
    $.createAlert = function(sAlertType, sAlertMessage) {
        return '<div class="alert alert-' + sAlertType + ' alert-dismissible fade show" role="alert">'
             +   sAlertMessage
             +   '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
             +     '<span aria-hidden="true">&times;</span>'
             +   '</button>'
             + '</div>';
    }
});
