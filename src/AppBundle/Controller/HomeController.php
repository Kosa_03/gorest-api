<?php

declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Form\ApiUsersType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * HomeController class
 */
class HomeController extends Controller
{
    /**
     * @Route("/", name="app_home")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $apiUsersForm = $this->createForm(ApiUsersType::class);

        return $this->render('default/controller/app_home.html.twig', [
            'title' => 'Tomasz Bukowski - Users Api Service',
            'users_form' => $apiUsersForm->createView()
        ]);
    }
}
