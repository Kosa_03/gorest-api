<?php

declare(strict_types=1);

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * ApiUsersType class
 */
class ApiUsersType extends AbstractType
{
    /**
     * @param FormFormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('users_list_username', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'form-input-search-user mr-1'
                ]
            ])
            ->add('users_list_search_user', ButtonType::class, [
                'label' => 'Search',
                'attr' => [
                    'class' => 'btn-outline-primary ml-1 mr-3'
                ]
            ])
            ->add('users_list_get_all_users', ButtonType::class, [
                'label' => 'Get Users',
                'attr' => [
                    'class' => 'btn-outline-success ml-3 mr-3'
                ]
            ])
            ->add('users_list_clear_table', ButtonType::class, [
                'label' => 'Clear Table',
                'attr' => [
                    'class' => 'btn-outline-danger ml-3'
                ]
            ])
            ->add('users_list_preview', ButtonType::class, [
                'label' => 'Preview',
                'attr' => [
                    'class' => 'btn-outline-primary mr-3'
                ]
            ])
            ->add('users_list_next', ButtonType::class, [
                'label' => 'Next',
                'attr' => [
                    'class' => 'btn-outline-primary ml-3'
                ]
            ])
        ;
    }
}
