# goREST API - User service

1. You need add this code into `parameters.yml` file.

```
api_url: https://gorest.co.in/public-api/users
api_key: <your_api_key>
```
